// Теоретичні питання:
// 1. number, string, boolean, null, undefined, symbol, bigint, object.
// 2. Перше нестроге, а друге - строге порівнювання, строге перевіряє також на тип даних, тому безпечніше користуватись саме ===.
// 3. Оператор це якась інструкція, яка вказує що треба зробити з операндом(аргументом) який знаходиться поряд з оператором.

// Завдання:

let userName;
let userAge;

while (!userName) {
  userName = prompt("Enter your name");
}
while (!userAge) {
  userAge = +prompt("Enter your age");
}
if (userAge < 18) {
  alert("You are not allowed to visit this website");
} else if (userAge > 22) {
  alert(`Welcome, ${userName}`);
} else {
  let userDecision = confirm("Are you sure you want to continue?");
  if (userDecision === false) {
    alert("You are not allowed to visit this website");
  } else {
    alert(`Welcome, ${userName}`);
  }
}